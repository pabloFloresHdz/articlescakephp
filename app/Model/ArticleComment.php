<?php
App::uses('AppModel', 'Model');
/**
 * ArticleComment Model
 *
 * @property Article $Article
 * @property Registration $Registration
 */
class ArticleComment extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Article' => array(
			'className' => 'Article',
			'foreignKey' => 'article_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Registration' => array(
			'className' => 'Registration',
			'foreignKey' => 'registration_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
