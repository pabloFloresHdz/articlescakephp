<?php
App::uses('AppModel', 'Model');
/**
 * Registration Model
 *
 * @property ArticleComment $ArticleComment
 * @property Article $Article
 */
class Registration extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'registration';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ArticleComment' => array(
			'className' => 'ArticleComment',
			'foreignKey' => 'registration_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Article' => array(
			'className' => 'Article',
			'foreignKey' => 'registration_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
